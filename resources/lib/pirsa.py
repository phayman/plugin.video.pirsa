#This file will define the tools needed to parse the PIRSA website 

# import xbmc
import requests
from bs4 import BeautifulSoup

URL = "http://pirsa.org/"
STREAM = "http://streamer2.perimeterinstitute.ca/mp4-med/"

#Define the options for the catchup --- WE WILL PULL THESE FROM THE SITE THIS LATER
# CATCHUP_TIMES = {"Weekly" : "week", "Bi-weekly" : "bi-weekly" , "Monthly" : "monthly"}
# CATCHUP_CAT = {"All" : "all" , 
#         "Condensed Matter" : "9" , 
#         "Quantum Foundations" : "2",
#         "Quantum Information Theory" : "1",
#         "Quantum Gravity" : "3",
#         "Quantum Fields and Strings" : "4",
#         "Particle Physics" : "5",
#         "Cosmology" : "6",
#         "Strong Gravity" : "10",
#         "Public Outreach" : "8",
#         "Mathematical Physics" : "11",
#         "Other" : "7"}
SERIES_CAT = {
        "All" : "ALL",
        "Quantum Foundations" : 25,
        "Quantum Fields and Strings" : 24, 
        "Cosmology" : 21, 
        "Quantum Gravity" : 26, 
        "Quantum Information" : 27,
        "Particle Physics" : 23,
        "Strong Gravity" : 28,
        "Condensed Matter" : 20,
        "Mathematical Physics" : 22,
        "Other" : 29,
        }
# TERMS = {"Winter Term" : 1,
#         "Spring Term" : 2,
#         "Fall Term" : 3,
#         "All" : ""}
COLLECTIONS_CAT = {
        "All" : "ALL",
        "Conference" : 37,
        "Course" : 35,
        "Other" : 38,
        }
MIN_YEAR = 2002
MAX_YEAR = 2021
ALL_YEARS = ""


### This is the big one, retrieves all the specific video info from each result page. ###

def video_info(soup):
    #Return value
    info = [] 

    #Get all the lectures in the given page
    # lectures = soup.find_all(class_="lecture")
    lectures = soup.find_all(class_="content")

    for lec in lectures:
       info.append({})
       info[-1]["title"] = lec.get_text()
       info[-1]["num"] = lec.find_previous("article")["about"]
       info[-1]["thumb"] = lec.find_previous("img")["src"]
       info[-1]["stream"] = STREAM+info[-1]["num"]+".mp4"
       info[-1]["speaker"] = "N/A"
       info[-1]["date"] =  "N/A"
       info[-1]["series"] =  "N/A"
       info[-1]["collection"] =  "N/A"
       info[-1]["abstract"] =  "N/A"

    return info 

    #For each lecture, extract all the info we can
    #for lec in lectures:
    #   info.append({})
    #   info[-1]["title"] = lec.get_text()
    #   info[-1]["num"] = lec.find_previous("b").get_text()
    #   info[-1]["thumb"] = lec.find_previous("img")["src"]
    #   info[-1]["stream"] = STREAM+info[-1]["num"]+".mp4"

    #   for div in lec.find_next_siblings("div"):
    #       if div.get_text().find("Speaker") >= 0:
    #           info[-1]["speaker"] = div.get_text()[12:]
    #       elif div.get_text().find("Date: ") >= 0:
    #           info[-1]["date"] = div.get_text()[6:]
    #       elif div.get_text().find("Series") >= 0:
    #           info[-1]["series"] = div.find_next().get_text()
    #       elif div.get_text().find("Collection") >= 0:
    #           info[-1]["collection"] = div.find_next().get_text()
    #       else:
    #           pass
           
    #   #Get full abstract
    #   newSoup = BeautifulSoup(requests.get(URL+info[-1]["num"]).text, features="html.parser")
    #   if newSoup.p == None:
    #       info[-1]["abstract"] = "No abstract provided."
    #   else:
    #       info[-1]["abstract"] = newSoup.p.get_text()

    # return info 


### Pass the info for all the options ###

def catchup(time, cat):

    r = requests.post(URL, data = {'catchup':CATCHUP_TIMES[time],'area':CATCHUP_CAT[cat],'search':'do catchup','p':'search','subCatchup':''})
    soup = BeautifulSoup(r.text, features="html.parser")

    return video_info(soup) 

def direct_pirsa(num):

    #Setup the parsers
    DIR_URL = URL+num
    r = requests.get(DIR_URL)
    soup = BeautifulSoup(r.text, features="html.parser")
    
    return video_info(soup) 

def series(cat, term, year):

    if(year == "all"): year = ""
    r = requests.post(URL, data = {'series':SERIES_CAT[cat],'term':TERMS[term],'series_year':year,'search':'view','p':'series'})
    soup = BeautifulSoup(r.text, features="html.parser")

    return video_info(soup) 

def collections(typ, year):

    if(year == "all"): year = ""
    r = requests.post(URL, data = {'collection_type':COLLECTIONS_CAT[typ],'collection_year':year,'search':'view','p':'collection'})
    soup = BeautifulSoup(r.text, features="html.parser")

    cols = {}

    for entry in soup.find_all("a", class_="collection_link"):
        cols[entry.get_text()] = entry.get("href")[1:]

    return cols 

def view_collection(num):

    DIR_URL = URL+num
    r = requests.get(DIR_URL)
    soup = BeautifulSoup(r.text, features="html.parser")
    
    return video_info(soup) 

def outreach(year):

    if(year == "all"): year = ""
    r = requests.post(URL, data = {'collection_type':12,'collection_year':year,'search':'view','p':'collection'})
    soup = BeautifulSoup(r.text, features="html.parser")

    cols = {}

    for entry in soup.find_all("a", class_="collection_link"):
        cols[entry.get_text()] = entry.get("href")[1:]

    return cols 

def search(typ, text):

    #Setup the parsers
    r = requests.post(URL, {"keywords":text, "type":typ, "search":"Go", "p":"search"})
    soup = BeautifulSoup(r.text, features="html.parser")
    
    return video_info(soup) 


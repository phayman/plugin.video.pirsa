# -*- coding: utf-8 -*-

import routing
import logging
import xbmcaddon
import resources.lib.pirsa as pirsa
import xbmc
from resources.lib import kodiutils
from resources.lib import kodilogging
from xbmcgui import ListItem, Dialog, INPUT_ALPHANUM, INPUT_NUMERIC, Window 
from xbmcplugin import addDirectoryItem, endOfDirectory, setResolvedUrl, addSortMethod, SORT_METHOD_LABEL_IGNORE_THE, SORT_METHOD_DATE


ADDON = xbmcaddon.Addon()
logger = logging.getLogger(ADDON.getAddonInfo('id'))
kodilogging.config()
plugin = routing.Plugin()

# The main action for the main index 
@plugin.route('/')
def index():
    addDirectoryItem(plugin.handle, plugin.url_for(
        catchup), ListItem("Catchup"), True)
        # catchup_time), ListItem("Catchup"), True)
    addDirectoryItem(plugin.handle, plugin.url_for(
        pirsa_no), ListItem("Pirsa No."), True)
    addDirectoryItem(plugin.handle, plugin.url_for(
        series), ListItem("Series"), True)
    addDirectoryItem(plugin.handle, plugin.url_for(
        collections), ListItem("Collections"), True)
    addDirectoryItem(plugin.handle, plugin.url_for(
        outreach), ListItem("Outreach"), True)
    addDirectoryItem(plugin.handle, plugin.url_for(
        simplesearch), ListItem("Search"), True)
    # addDirectoryItem(plugin.handle, "",  #TODO
    #         ListItem("Saved Collections"), True)
    # addDirectoryItem(plugin.handle, "",  #TODO
    #         ListItem("Saved Videos"), True)

    endOfDirectory(plugin.handle, updateListing=True)

### Catchup ###

# The main action for the catchup option
#This function is referenced for each of the following urls
@plugin.route('/catchup')
@plugin.route('/catchup/<time>')
@plugin.route('/catchup/<time>/<cat>')
def catchup(time = None, cat = None):

    addSortMethod(plugin.handle, SORT_METHOD_LABEL_IGNORE_THE)
    #If we're coming here from the main page, we need to populate the time options
    if time == None:
        for t in pirsa.CATCHUP_TIMES:
            addDirectoryItem(plugin.handle, plugin.url_for(catchup, t), ListItem(t), True)
    #If we're coming here from the time page, we need to populate the category options
    elif cat == None:
        for c in pirsa.CATCHUP_CAT:
            addDirectoryItem(plugin.handle, plugin.url_for(catchup, time = time, cat = c), ListItem(c), True)
    #And if we're clicking on a category, we need to populate the list from the PIRSA webpage.
    else:
        info = pirsa.catchup(time, cat)
        item = vid_list_items(info)
        for entry in item:
            addDirectoryItem(plugin.handle, plugin.url_for(play_vid, entry.getProperty("StreamLink")), entry, False)
        addSortMethod(plugin.handle, SORT_METHOD_DATE)

    endOfDirectory(plugin.handle)

# The main action for the pirsa number option
@plugin.route('/pirsa')
def pirsa_no():
    dialog = Dialog()
    d = dialog.input('Enter PIRSA #: ',type=INPUT_NUMERIC)
    xbmc.log(d)
    info = pirsa.direct_pirsa(d)

    item = vid_list_items(info)

    addDirectoryItem(plugin.handle, plugin.url_for(play_vid, info[0]["stream"]), item[0], False)
    
    endOfDirectory(plugin.handle)

# The main action for the series option
#This function is referenced for each of the following urls
@plugin.route('/series')
@plugin.route('/series/<cat>')
@plugin.route('/series/<cat>/<term>')
@plugin.route('/series/<cat>/<term>/<year>')
def series(cat = None, term = None, year = None):

    addSortMethod(plugin.handle, SORT_METHOD_LABEL_IGNORE_THE)
    #If we're coming here from the main page, we need to populate the time options
    if cat == None:
        for c in pirsa.SERIES_CAT:
            addDirectoryItem(plugin.handle, plugin.url_for(series, c), ListItem(c), True)
    #If we're coming here from the category page, we need to populate the term options
    elif term == None:
        for t in pirsa.TERMS:
            addDirectoryItem(plugin.handle, plugin.url_for(series, cat = cat, term = t), ListItem(t), True)
    #If we're coming here from the term page, we need to populate the year options
    elif year == None:
        addDirectoryItem(plugin.handle, plugin.url_for(series, cat = cat, term = term, year = "all"), ListItem("All"), True)
        for y in range(pirsa.MIN_YEAR, pirsa.MAX_YEAR+1):
            addDirectoryItem(plugin.handle, plugin.url_for(series, cat = cat, term = term, year = y), ListItem(str(y)), True)
    #And if we're clicking on a category, we need to populate the list from the PIRSA webpage.
    else:
        info = pirsa.series(cat, term, year)
        item = vid_list_items(info)
        for entry in item:
            addDirectoryItem(plugin.handle, plugin.url_for(play_vid, entry.getProperty("StreamLink")), entry, False)
        addSortMethod(plugin.handle, SORT_METHOD_DATE)

    endOfDirectory(plugin.handle)

# The main action for the collections option
#This function is referenced for each of the following urls
@plugin.route('/collections')
@plugin.route('/collections/<typ>')
@plugin.route('/collections/<typ>/<year>')
@plugin.route('/collections/<typ>/<year>/<col>')
def collections(typ = None, year = None, col = None):

    addSortMethod(plugin.handle, SORT_METHOD_LABEL_IGNORE_THE)
    #If we're coming here from the main page, we need to populate the time options
    if typ == None:
        for t in pirsa.COLLECTIONS_CAT:
            addDirectoryItem(plugin.handle, plugin.url_for(collections, typ = t), ListItem(t), True)
    elif year == None:
        addDirectoryItem(plugin.handle, plugin.url_for(collections, year = "all"), ListItem("All"), True)
        for y in range(pirsa.MIN_YEAR, pirsa.MAX_YEAR+1):
            addDirectoryItem(plugin.handle, plugin.url_for(collections, typ = typ, year = y), ListItem(str(y)), True)
    elif col == None:
        cols = pirsa.collections(typ,year)
        for entry in cols:
            addDirectoryItem(plugin.handle, plugin.url_for(collections, typ=typ, year = year, col = cols[entry]), ListItem(entry), True)
    #And if we're clicking on a category, we need to populate the list from the PIRSA webpage.
    else:
        info = pirsa.view_collection(col)
        item = vid_list_items(info)
        for entry in item:
            addDirectoryItem(plugin.handle, plugin.url_for(play_vid, entry.getProperty("StreamLink")), entry, False)
        addSortMethod(plugin.handle, SORT_METHOD_DATE)

    endOfDirectory(plugin.handle)
# The main action for the outreach option
#This function is referenced for each of the following urls
@plugin.route('/outreach')
@plugin.route('/outreach/<year>')
@plugin.route('/outreach/<year>/<col>')
def outreach(year = None, col = None):

    addSortMethod(plugin.handle, SORT_METHOD_LABEL_IGNORE_THE)
    #If we're coming here from the main page, we need to populate the time options
    if year == None:
        addDirectoryItem(plugin.handle, plugin.url_for(outreach, year = "all"), ListItem("All"), True)
        for y in range(pirsa.MIN_YEAR, pirsa.MAX_YEAR+1):
            addDirectoryItem(plugin.handle, plugin.url_for(outreach, year = y), ListItem(str(y)), True)
    elif col == None:
        cols = pirsa.outreach(year)
        for entry in cols:
            addDirectoryItem(plugin.handle, plugin.url_for(outreach, year = year, col = cols[entry]), ListItem(entry), True)
    #And if we're clicking on a category, we need to populate the list from the PIRSA webpage.
    else:
        info = pirsa.view_collection(col)
        item = vid_list_items(info)
        for entry in item:
            addDirectoryItem(plugin.handle, plugin.url_for(play_vid, entry.getProperty("StreamLink")), entry, False)
        addSortMethod(plugin.handle, SORT_METHOD_DATE)

    endOfDirectory(plugin.handle)

#This function is referenced for each of the following urls
@plugin.route('/simplesearch')
@plugin.route('/simplesearch/<typ>')
def simplesearch(typ = None):

    #If we're coming here from the main page, we need to populate the time options
    if typ == None:
        addDirectoryItem(plugin.handle, plugin.url_for(simplesearch, typ = "sp"), ListItem("Speakers"), True)
        addDirectoryItem(plugin.handle, plugin.url_for(simplesearch, typ = "ti"), ListItem("Titles/Abstracts"), True)
    else:
        dialog = Dialog()
        d = dialog.input("Search " + str("Speakers" if typ == "sp" else "Titles/Abstracts") + " : ", type=INPUT_ALPHANUM)
        xbmc.log(d)
        if d:
            info = pirsa.search(typ, d)
            item = vid_list_items(info)
            for entry in item:
                addDirectoryItem(plugin.handle, plugin.url_for(play_vid, entry.getProperty("StreamLink")), entry, False)
    endOfDirectory(plugin.handle)

@plugin.route('/pirsa/<path:url>')
def play_vid(url):
    vid = ListItem(url)
    vid.setProperty('IsPlayable','true')
    vid.setPath(url)
    setResolvedUrl(plugin.handle, True, vid)

#Make a list item from each video parsed
def vid_list_items(info):
    items = []
    for vid in info:
        items.append(ListItem(vid["title"]))
        items[-1].setProperty('IsPlayable', 'true')
        items[-1].setArt({'thumb':vid["thumb"]})
        items[-1].setInfo('video',{ "aired" : vid["date"], 
        "plot" : vid["abstract"], 
        "cast" : [vid["speaker"]]})
        if "series" in vid:
            items[-1].setInfo('video',{"set" : vid["series"]})
        elif "collection" in vid:
            items[-1].setInfo('video',{"set" : vid["collection"]})
        items[-1].setProperty("StreamLink",vid["stream"])

    return items

def run():
    plugin.run()

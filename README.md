PIRSA Plugin
============

This is a plugin to browse and watch videos from the Perimeter Institute Recorded Seminar Archive (pirsa.org). 

What works:
----------

- Playing videos from pirsa.org using the direct PIRSA number search

To-do:
------

- Fix everything that was broken with the PIRSA website update
- Implement saved video and collection lists
- Implement advanced search (maybe)
- Pretty-up/comment the code
 


